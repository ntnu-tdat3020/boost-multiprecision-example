# Boost.Multiprecision example

## Prerequisites
  * Linux or MacOS
  * The C++ IDE [juCi++](https://github.com/cppit/jucipp) should be installed.

## Installing dependencies
You should already have the needed boost library installed

## Compiling and running
In a terminal:
```sh
git clone https://gitlab.com/ntnu-tdat3020/boost-multiprecision-example
juci boost-multiprecision-example
```

Choose Compile and Run in the Project menu.